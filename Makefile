clean:
	rm -rf .aws-sam

build-eth:
	sam build --template templates/eth/template.yaml
deploy-eth:
	sam deploy --config-file deployConfigs/eth-miner.toml

build-etc:
	sam build --template templates/etc/template.yaml
deploy-etc:
	sam deploy --config-file deployConfigs/etc-miner.toml


build-beam:
	sam build --template templates/beam/template.yaml
deploy-beam:
	sam deploy --config-file deployConfigs/beam.toml



build-raven:
	sam build --template templates/raven/template.yaml
deploy-raven:
	sam deploy --config-file deployConfigs/raven.toml