# OVERVIEW

* Contains templates that can be deployed to AWS on GPU instances
    - ETH (working)
    - ETC (working but something wrong with hashing algorithm settings)
    - BEAM (working)
    - RVN (working)
    - ZEC (in progress)
* The templates deploy an EC2 autoscaling group that has a goal to maintain a template defined number of spot instance miners active (default is 5)
* When the autoscaling group finds a spot instance available for <= the maximum defined spot price for the instance type you select, it will provision 
it and run a user data script that sets it up as a miner
  
```yaml
  SpotPriceMap:
    g4dn.xlarge:
      MaxPrice: 0.16
    g4dn.2xlarge:
      MaxPrice: 0.23
    g4dn.12xlarge:
      MaxPrice: 1.57
    p3.2xlarge:
      MaxPrice: 0.92
    p3.8xlarge:
      MaxPrice: 3.68
    p3.16xlarge:
      MaxPrice: 7.36
```
* If the spot instance terminates, the autoscaling group returns to searching for the next available and automatically provisions it


# Template Parameters

* Wallet Address: enter the wallet address you want your profits deposited to in the ```${Wallet}``` template parameter
* VPC Id: enter the default VPC ID for the region you are deploying the stack into
* Desired subnets: in the template, look in the ```MinerAsg::Properties::VPCZoneIdentifier``` resource definition and update the subnets id's to ones that exist in your default VPC
```yaml
  MinerAsg:
    Type: AWS::AutoScaling::AutoScalingGroup
    Properties:
      LaunchTemplate:
        LaunchTemplateId: !Ref LaunchTemplate
        Version: !GetAtt LaunchTemplate.LatestVersionNumber
      MinSize: 0
      MaxSize: !Ref InstanceCount
      DesiredCapacity: !Ref InstanceCount
      CapacityRebalance: false
      VPCZoneIdentifier:
        - subnet-a88ffed3
        - subnet-1202347b
        - subnet-272e906a

      HealthCheckGracePeriod: 300
      HealthCheckType: EC2
      TerminationPolicies:
      - ClosestToNextInstanceHour
      NotificationConfigurations:
      - TopicARN: !Ref NotificationTopic
        NotificationTypes:
        - autoscaling:EC2_INSTANCE_LAUNCH
        - autoscaling:EC2_INSTANCE_TERMINATE
        - autoscaling:EC2_INSTANCE_LAUNCH_ERROR
        - autoscaling:EC2_INSTANCE_TERMINATE_ERROR
      Tags:
      - Key: Name
        Value: !Ref AWS::StackName
        PropagateAtLaunch: true
```
* The default instance type is g4dn.xlarge because it is the most cost effective (most hash out for least price)

# Deployment

## Requirements:
* [Homebrew](https://docs.brew.sh/Installation)
* Make: ```brew install make```
* aws-sam-cli: ```brew tap aws/tap
brew install aws-sam-cli
sam --version```
  
## Steps
* To deploy, use the following commands. The following example is for RVN but other miners follow a similar pattern:
```makefile
make build-raven
sam deploy --guided
```

* When running the guided deployment, you can save the configuration file in a new folder called deployConfig. See the Makefile deploy command for the deployment config file naming convention:
 - RVN: deployConfig/raven.toml
 - ETH: deployConfig/eth.toml
 - BEAM: deployConfig/beam.toml

* The next time you deploy the stack with modification, you will be able to run these commands to deploy:
````makefile
make clean
make build-raven
make deploy-raven
````