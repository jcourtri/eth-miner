import boto3
from libs.constants import *
import libs.utils as utils
import time
import datetime

security_group_name = utils.randString() + utils.randString()

user_data = """#!/bin/bash
sudo apt update
sudo apt-get install xz-utils
wget -O miner.tar.xz {}
tar -xvf miner.tar.xz
./miner --algo etchash --server {} --user {}.{}""".format(GMINER_URL, MINING_POOL_ADDRESS, ETC_WALLET_ADDRESS,
                                                              ETC_WORKER_NAME)

ec2 = boto3.resource('ec2',
                     aws_access_key_id=AWS_ACCESS_KEY,
                     aws_secret_access_key=AWS_SECRET_KEY,
                     region_name=AWS_DEFAULT_REGION)
ec2_client = boto3.client('ec2',
                          aws_access_key_id=AWS_ACCESS_KEY,
                          aws_secret_access_key=AWS_SECRET_KEY,
                          region_name=AWS_DEFAULT_REGION)
ssm_client = boto3.client('ssm',
                          aws_access_key_id=AWS_ACCESS_KEY,
                          aws_secret_access_key=AWS_SECRET_KEY,
                          region_name=AWS_DEFAULT_REGION)
# create VPC
print("Creating VPC and Security Group")
response = ec2_client.describe_vpcs()
vpc_id = response.get('Vpcs', [{}])[0].get('VpcId', '')
response = ec2_client.create_security_group(GroupName=security_group_name,
                                            Description="RTMP_SG_{}".format(utils.randString()),
                                            VpcId=vpc_id)
security_group_id = response['GroupId']
print('Security Group Created %s in vpc %s.' % (security_group_id, vpc_id))
data = ec2_client.authorize_security_group_ingress(
    GroupId=security_group_id,
    IpPermissions=[
        {'IpProtocol': 'tcp',
         'FromPort': 22,
         'ToPort': 22,
         'IpRanges': [{'CidrIp': '0.0.0.0/0'}]}
    ])
print('Ingress Successfully Set %s' % data)

# create key pair
print("Creating Key Pair")
key_pair_name = 'ec2-keypair-{}'.format(utils.randString())
outfile = open('keys/ec2-keypair.pem', 'w')
key_pair = ec2.create_key_pair(KeyName=key_pair_name)

KeyPairOut = str(key_pair.key_material)
print("Key Pair data: {}".format(KeyPairOut))
outfile.write(KeyPairOut)

# create a new EC2 instance
print("Creating EC2 Instance")
instance = ec2.create_instances(
    ImageId=EC2_AMI,
    MinCount=1,
    MaxCount=1,
    InstanceType=INSTANCE_TYPE,
    KeyName=key_pair_name,
    UserData=user_data,
    SecurityGroupIds=[security_group_id],
    InstanceMarketOptions={
        'MarketType': 'spot',
        'SpotOptions': {
            'MaxPrice': '0.10',
            'SpotInstanceType': 'persistent',
            'BlockDurationMinutes': 123,
            'ValidUntil': datetime.datetime.now() + datetime.timedelta(hours=4),
            'InstanceInterruptionBehavior': 'hibernate'
        }
    }
)
instance = instance[0]

print("Created Instance {}".format(instance))
instanceState = instance.state['Name']
instanceRunning = False
timesChecked = 0
while not instanceRunning and timesChecked < 10:
    print("Checking Instance State")
    time.sleep(30)
    instanceState = instance.state['Name']
    if instanceState.lower() == 'running':
        instanceRunning = True
    print("Instance State: {}".format(instanceState))
    print("Times Checked: {}".format(timesChecked))
    timesChecked += 1
    instance.reload()
if not instanceRunning:
    print("Instance not running after 5 minutes, check UI")
else:
    print("Instance address: {}".format(instance.public_dns_name))
    print("Instance IP: {}".format(instance.public_ip_address))
